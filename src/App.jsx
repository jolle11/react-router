import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { About, Home, Navbar, Profile, NotFound } from './components';
import './App.scss';

function App() {
    return (
        <Router>
            <div className="app">
                <Navbar></Navbar>
                <Routes>
                    <Route exact path="/" element={<Home></Home>}></Route>
                    <Route path="/home/*" element={<Home></Home>}></Route>
                    <Route path="/about" element={<About></About>}></Route>
                    <Route path="/profile" element={<Profile></Profile>}></Route>
                    <Route path="*" element={<NotFound></NotFound>}></Route>
                </Routes>
            </div>
        </Router>
    );
}

export default App;
