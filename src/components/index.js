import About from './About/About';
import Home from './Home/Home';
import Profile from './Profile/Profile';
import Navbar from './Navbar/Navbar';
import TestForm from './TestForm/TestForm';
import NotFound from './NotFound/NotFound';

export { About, Home, Navbar, Profile, TestForm, NotFound };
