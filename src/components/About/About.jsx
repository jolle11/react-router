// import { useLocation, useNavigate, useParams } from 'react-router-dom';

const About = () => {
    // let location = useLocation(); // object with information about the actual route
    // let navigate = useNavigate(); // to redirect to specified URL
    // let params = useParams();

    // console.log(navigate);

    return (
        <>
            <h1>About</h1>
            <h2 onClick={() => window.history.pushState('Look at me!', undefined, '/profile')}>Click on me!</h2>
        </>
    );
};

export default About;
