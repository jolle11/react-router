import './Navbar.scss';
import { Link } from 'react-router-dom';

const Navbar = () => {
    return (
        <div className="navbar">
            <Link to="/home">
                <button>Home</button>{' '}
            </Link>
            <Link to="/about">
                <button>About</button>{' '}
            </Link>
            <Link to="/profile">
                <button>Profile</button>{' '}
            </Link>
        </div>
    );
};

export default Navbar;
