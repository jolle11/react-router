import { Link, Routes, Route } from 'react-router-dom';
import { TestForm } from '../../components';

const One = (props) => <div>One</div>;

const Home = () => {
    return (
        <>
            <h1>Home</h1>
            <Link to="/home/one">
                <button>One</button>
            </Link>
            <Link to="/home/form">
                <button>TestForm</button>
            </Link>
            <Routes>
                <Route path="/one" element={<One />}></Route>
                <Route path="/form" element={<TestForm />}></Route>
            </Routes>
        </>
    );
};

export default Home;
